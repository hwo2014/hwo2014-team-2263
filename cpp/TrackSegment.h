#ifndef HWO_TRACK_SEGMENT_H
#define HWO_TRACK_SEGMENT_H

#include <string>
#include <iostream>
#include <jsoncons/json.hpp>

class TrackSegment {
public:
  float length;
  bool canSwitch;
  float radius;
  float angle;

public:
  TrackSegment(const jsoncons::json& data);
  std::string description();
};

#endif