#include "game_logic.h"
#include "protocol.h"
#include <math.h>

using namespace hwo_protocol;

// Constants.
const float kMinimumSpeed = 16.125; // 0 - 100. > faster.
const float kDesiredSpeedFactor = 2.125; // 0 - 100. > faster.
const float kAlarmingSpeedDifference = 0.0f; // -100 - kMinimumSpeed. > faster.
const float kBalancingThrottle = 0.0; // 0 - 1. > faster.
const float kHighSpeedDifference = 60; // 0 - 100. < faster.
const float kMinimumThrottle = 0.505; // 0 - 1. > faster.

game_logic::game_logic()
  : action_map
    {
      { "join", &game_logic::on_join },
      { "yourCar", &game_logic::on_car_assigned },
      { "gameInit", &game_logic::on_game_init },
      { "gameStart", &game_logic::on_game_start },
      { "carPositions", &game_logic::on_car_positions },
      { "turboAvailable", &game_logic::on_turbo_available },
      { "turboStart", &game_logic::on_turbo_start },
      { "crash", &game_logic::on_crash },
      { "gameEnd", &game_logic::on_game_end },
      { "error", &game_logic::on_error }
    }
{
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg) {
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end()) {
    return (action_it->second)(this, data);
  } else {
    std::cout << "Unknown message type: " << msg_type << std::endl;
    return { make_ping() };
  }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data) {
  std::cout << "Joined" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_assigned(const jsoncons::json& data) {
  _carColor = data["color"].as<std::string>();
  std::cout << "Car assigned: " << _carColor << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data) {
  _turbo = false;
  _turboMultiplier = 0;
  for (int i = 0; i < data["race"]["track"]["pieces"].size(); i++) {
    TrackSegment segment(data["race"]["track"]["pieces"][i]);
    _trackSegments.push_back(segment);
  }
  _currentState.tick = 0;
  _currentState.index = 0;
  determineThrottle(findNextTurn());
  return { make_ping() };
}

float game_logic::findNextTurn() {
  float distanceToTurn = _trackSegments[_currentState.index].length -
      _currentState.distance;
  for (int j = 1; j < _trackSegments.size(); j++) {
    int nextIndex = (_currentState.index + j) % _trackSegments.size();
    TrackSegment followSegment = _trackSegments[nextIndex];
    if (followSegment.angle != 0) {
      if (followSegment.angle > 0) {
        _turnIntentLeft = true;
      } else {
        _turnIntentLeft = false;
      }
      if (j >= 10 && _isTurboAvailable) {
        _isTurboAvailable = false;
        _turbo = true;
      }
      return j;
    }
    distanceToTurn += followSegment.length;
  }
  return 0;
}

void game_logic::determineThrottle(float distanceToTurn) {
  // if (segmentsUntilTurn > 8) {
  //   segmentsUntilTurn = 8;
  // }
  // _throttle = kSafeThrottle + (float)(kMaxThrottle - kSafeThrottle) *
  //     (float)((float)segmentsUntilTurn / 8.0);
  float desiredSpeed = distanceToTurn * kDesiredSpeedFactor + 
      kMinimumSpeed;
  float speedDiff = desiredSpeed - _speed;
  if (speedDiff < kAlarmingSpeedDifference) {
    _throttle = kBalancingThrottle;
  } else {
    if (speedDiff > kHighSpeedDifference) {
      _throttle = 1.0;
    } else {
      _throttle = kMinimumThrottle +
          ((speedDiff / kHighSpeedDifference) * (1 - kMinimumThrottle));
    }
  }
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data) {
  std::cout << "Race started" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector
    game_logic::on_turbo_available(const jsoncons::json& data) {
  std::cout << "Turbo available" << data << std::endl;
  _isTurboAvailable = true;
  _turboMultiplier = data["turboFactor"].as<double>();
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_turbo_start(const jsoncons::json& data) {
  _turbo = false;
  std::cout << "Pow pow!" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector
    game_logic::on_car_positions(const jsoncons::json& data) {
  State newState;
  newState.tick = _currentState.tick++;
  for (int i = 0; i < data.size(); i++) {
    const jsoncons::json& car = data[i];
    if (car["id"]["color"].as<std::string>() == _carColor) {
      newState.index = car["piecePosition"]["pieceIndex"].as<int>();
      newState.distance =
          car["piecePosition"]["inPieceDistance"].as<double>();
      if (_currentState.index == newState.index) {
        _speed = newState.distance - _currentState.distance;
      } else {
        _speed = 0;
        for (int j = _currentState.index; j <= newState.index; j++) {
          if (j == _currentState.index) {
            _speed += (_trackSegments[j].length - _currentState.distance);
          } else {
            _speed += _trackSegments[j].length;
          }
        }
      }
      _speed = _speed / 1;
      _currentState = newState;
      determineThrottle(findNextTurn());
    }
  }
  if (_turbo) {
    return { make_throttle(_throttle),
             make_switch(_turnIntentLeft),
             make_turbo("pow pow!")
    };
  } else {
    return { make_throttle(_throttle), make_switch(_turnIntentLeft) };
  }
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
  std::cout << "Someone crashed" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
  std::cout << "Race ended" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  return { make_ping() };
}
