#include "TrackSegment.h"
#include <boost/lexical_cast.hpp>

TrackSegment::TrackSegment(const jsoncons::json& data) {
  try {
    this->length = data["length"].as<double>();
  } catch (const std::exception& e) {
    this->length = 0;
  }
  try {
    this->canSwitch = data["switch"].as<bool>();
  } catch (const std::exception& e) {
    this->canSwitch = false;
  }
  try {
    this->radius = data["radius"].as<double>();
  } catch (const std::exception& e) {
    this->radius = 0;
  }
  try {
    this->angle = data["angle"].as<double>();
  } catch (const std::exception& e) {
    this->angle = 0;
  }
}

std::string TrackSegment::description() {
  return "l:" + boost::lexical_cast<std::string>(this->length) + 
         ",s:" + boost::lexical_cast<std::string>(this->canSwitch) +
         ",r:" + boost::lexical_cast<std::string>(this->radius) +
         ",a:" + boost::lexical_cast<std::string>(this->angle);
}